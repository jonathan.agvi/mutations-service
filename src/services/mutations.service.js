// eslint-disable-next-line import/no-extraneous-dependencies
import DynamoDB from 'aws-sdk/clients/dynamodb';
import SNS from 'aws-sdk/clients/sqs';
import { v4 as uuidv4 } from 'uuid';

const dynamo = new DynamoDB.DocumentClient({ region: process.env.REGION });
const sns = new SNS();

export const saveDna = async (dna) => {
    const { Attributes } = await dynamo.put({
        TableName: process.env.DNA_RESULTS_TABLE_NAME,
        Item: dna,
        ConditionExpression: 'dna != :dna',
        ExpressionAttributeValues: { ':dna': dna },
    }).promise();
    return Attributes;
};

export const saveDnaList = async (dnaList) => dynamo.batchWrite({
    RequestItems: {
        [process.env.DNA_RESULTS_TABLE_NAME]: dnaList,
    },
}).promise();

export const getDna = async (dna) => {
    const { Item } = await dynamo.get({
        TableName: process.env.DNA_RESULTS_TABLE_NAME,
        Key: { id: dna.join('') },
    }).promise();
    return Item;
};

export const increaseMutationsCount = async (countMutation, countNoMutation) => {
    const now = new Date();
    const { Attributes } = await dynamo.update({
        TableName: process.env.DNA_STATS_TABLE_NAME,
        Key: { id: 'counter' },
        UpdateExpression: 'ADD count_mutations :countMutation, count_no_mutations :countNoMutation SET #uAt = :uAt',
        ExpressionAttributeValues: {
            ':countMutation': countMutation,
            ':countNoMutation': countNoMutation,
            ':uAt': now.toISOString(),
        },
        ExpressionAttributeNames: {
            '#uAt': 'updatedAt',
        },
        ReturnValues: 'ALL_NEW',
    }).promise();
    return Attributes;
};

export const getStats = async () => {
    const { Item } = await dynamo.get({
        TableName: process.env.DNA_STATS_TABLE_NAME,
        AttributesToGet: ['count_mutations', 'count_no_mutations'],
        Key: { id: 'counter' },
    }).promise();
    return Item || { count_mutations: 0, count_no_mutations: 0 };
};

export const sendDnaToSQS = async (dnaData) => {
    const { MessageId } = await sns.sendMessage({
        QueueUrl: process.env.DNA_REGISTER_SQS_URL,
        MessageBody: JSON.stringify(dnaData),
        MessageGroupId: uuidv4(),
    }).promise();

    return MessageId;
};
