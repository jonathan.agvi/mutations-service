/* eslint-disable max-len */
function buildResponse(statusCode, message, data, errors, customHeaders) {
    const headers = {
        'Content-Type': 'application/json',
        ...customHeaders,
    };
    return {
        statusCode,
        headers,
        body: JSON.stringify({
            message,
            data,
            errors,
        }),
    };
}

export const success = (message, data, statusCode = 200, customHeaders = {}) => buildResponse(statusCode, message, data, undefined, customHeaders);

export const failure = (message, errors, statusCode = 500, customHeaders = {}) => buildResponse(statusCode, message, undefined, errors, customHeaders);
