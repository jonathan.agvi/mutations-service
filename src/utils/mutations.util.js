const regEx = /([A,T,G,C])\1{3,}/g;

const getHorizontallyMutations = (dnaChain) => dnaChain.match(regEx) || [];

const getVerticallyMutations = (dna, rowIndex) => {
    const dnaVertically = dna.reduce((prev, curr) => prev + curr.charAt(rowIndex), '');
    return getHorizontallyMutations(dnaVertically);
};

const getObliquelyMutations = (dna, rowIndex) => {
    let dnaUp = '';
    let dnaDown = '';
    let dnaInverseUp = '';
    let dnaInverseDown = '';

    for (let i = 0; i < dna.length; i++) {
        if (dna.length - (rowIndex + 1) < 3) { break; } // Avoid iterating when not is necessary
        const dnaUpAux = dna[i];
        const dnaDownAux = dna[rowIndex + i + 1];

        dnaUp += dnaUpAux.charAt(rowIndex + i);
        dnaInverseUp += dnaUpAux.split('').reverse().join('').charAt(rowIndex + i);

        if (dnaDownAux) {
            dnaDown += dnaDownAux.charAt(i);
            dnaInverseDown += dnaDownAux.split('').reverse().join('').charAt(i);
        }
    }
    return [
        ...getHorizontallyMutations(dnaUp),
        ...getHorizontallyMutations(dnaDown),
        ...getHorizontallyMutations(dnaInverseUp),
        ...getHorizontallyMutations(dnaInverseDown),
    ];
};

/**
 * Get DNA mutations
 * @param {string[]} dna array for DNA fragments
 * @returns {string []} returns only DNA fragments mutatons
 */
export const getMutations = (dna) => {
    if (dna.length < 4) {
        throw new Error('DNA must have at least 4 rows');
    }
    return dna.reduce((prev, curr, index) => {
        if (curr.length < dna.length) {
            throw new Error('The DNA is incorrectly');
        }
        // Horizontally DNA fragment
        const mutationsHorizontally = getHorizontallyMutations(curr);

        // Vertical DNA fragment
        const mutationsVertically = getVerticallyMutations(dna, index);

        // Oblique DNA fragment
        const mutationsObliquely = getObliquelyMutations(dna, index);

        return [...prev, ...mutationsHorizontally, ...mutationsVertically, ...mutationsObliquely];
    }, []);
};

/**
 * Get if DNA has mutations
 * @param {string[]} dna array for DNA fragments
 * @returns {boolean} returns if DNA has any mutation
 */
export const hasMutations = (dna) => {
    const mutations = getMutations(dna);
    return mutations.length > 0;
};
