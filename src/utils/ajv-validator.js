import Ajv from 'ajv';
import ajvErrors from 'ajv-errors';

/**
 * Wrapper class for AJV (Lamba Optimized)
 */
export class AjvValidator {
    constructor(schema, opts = { allErrors: true }) {
        const ajv = new Ajv(opts);
        ajvErrors(ajv);
        this.validationFn = ajv.compile(schema);
    }

    isValid(data) {
        return this.validationFn(data);
    }

    getErrors() {
        if (!this.validationFn.errors) {
            return null;
        }
        return this.validationFn.errors.map(({ instancePath, message }) => {
            const path = instancePath ? `'${instancePath}' ` : '';
            return `${path}${message}`;
        });
    }
}
