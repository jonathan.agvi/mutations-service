import { hasMutations } from '../utils/mutations.util';
import { getDna, sendDnaToSQS } from '../services/mutations.service';
import { failure, success } from '../utils/responses';
import { AjvValidator } from '../utils/ajv-validator';
import schema from '../../schema-validators/verify-mutation.json';

const ajv = new AjvValidator(schema);

export const main = async (event) => {
    const body = JSON.parse(event.body);
    if (!ajv.isValid(body)) {
        return failure('Payload errors', ajv.getErrors(), 422);
    }
    try {
        let hasMutation = false;
        const dna = await getDna(body.dna);
        if (dna) {
            hasMutation = dna.hasMutations;
        } else {
            hasMutation = hasMutations(body.dna);
            await sendDnaToSQS({
                dna: body.dna,
                hasMutations: hasMutation,
            });
        }
        return success(`DNA has ${hasMutation ? '' : 'no '}mutations`, undefined, hasMutation ? 403 : 200);
    } catch (error) {
        console.error('Error to verify DNA', error);
        return failure('Error to verify DNA', error.message);
    }
};
