import { increaseMutationsCount, saveDnaList } from '../services/mutations.service';

export const main = async (event) => {
    let countMutations = 0;
    let countNoMutations = 0;
    const dnaListPutRequests = event.Records.map(({ body }) => {
        const { dna, hasMutations } = JSON.parse(body);
        if (hasMutations) {
            countMutations += 1;
        } else {
            countNoMutations += 1;
        }
        return {
            PutRequest: {
                Item: { dna, hasMutations, id: dna.join('') },
            },
        };
    });
    const resultWrite = await saveDnaList(dnaListPutRequests);
    const result = await increaseMutationsCount(countMutations, countNoMutations);
    console.log('Result counter', result);
    console.log('Result save dna', resultWrite);
    return true;
};
