import { getStats } from '../services/mutations.service';
import { failure, success } from '../utils/responses';

export const main = async () => {
    try {
        const { count_mutations, count_no_mutations } = await getStats();
        const result = {
            count_mutations,
            count_no_mutations,
            ratio: count_mutations && count_no_mutations ? count_mutations / count_no_mutations : 0,
        };
        return success('Stats obtained successfully', result);
    } catch (error) {
        console.error('Error to get stats', error);
        return failure('Error to get stats', error.message);
    }
};
