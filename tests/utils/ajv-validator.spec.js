const { AjvValidator } = require('../../src/utils/ajv-validator');

const schema = {
    type: 'object',
    properties: {
        foo: { type: 'integer' },
        bar: { type: 'string', nullable: true },
    },
    required: ['foo'],
    additionalProperties: false,
};

describe('AJV Validator Class', () => {
    const ajv = new AjvValidator(schema);
    it('Should return Object AjvValidator', () => {
        expect(ajv).toBeInstanceOf(AjvValidator);
    });

    it('Method isValid should return true', () => {
        const data = {
            foo: 1,
        };
        expect(ajv.isValid(data)).toBe(true);
    });

    it('Method isValid should return false', () => {
        const data = {
            bar: 1,
        };
        expect(ajv.isValid(data)).toBe(false);
    });

    it('Method getErrors should return array of errors', () => {
        ajv.isValid({
            bar: 1,
        });
        expect(ajv.getErrors()).toEqual(['must have required property \'foo\'', '\'/bar\' must be string']);
    });

    it('Method getErrors should return null', () => {
        ajv.isValid({
            foo: 1,
        });
        expect(ajv.getErrors()).toBe(null);
    });
});
