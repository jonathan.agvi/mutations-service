import { success, failure } from '../../src/utils/responses';

const customHeaders = {
    'api-key': 'api_key_secret',
    'x-user': 299,
};

describe('Return success response', () => {
    it('Should return successful response with customHeaders', () => {
        expect(success('Message', 'Data', 200, customHeaders)).toEqual({
            body: '{"message":"Message","data":"Data"}',
            headers: {
                'Content-Type': 'application/json',
                'api-key': 'api_key_secret',
                'x-user': 299,
            },
            statusCode: 200,
        });
    });
    it('Should return successful response without customHeaders', () => {
        expect(success('Message', 'Data', 200)).toEqual({
            body: '{"message":"Message","data":"Data"}',
            headers: {
                'Content-Type': 'application/json',
            },
            statusCode: 200,
        });
    });
});

describe('Return failure response', () => {
    it('Should return failure response with customHeaders', () => {
        expect(failure('Message', ['Data'], 500, customHeaders)).toEqual({
            body: '{"message":"Message","errors":["Data"]}',
            headers: {
                'Content-Type': 'application/json',
                'api-key': 'api_key_secret',
                'x-user': 299,
            },
            statusCode: 500,
        });
    });
    it('Should return failure response without customHeaders', () => {
        expect(failure('Message', ['Data'], 500)).toEqual({
            body: '{"message":"Message","errors":["Data"]}',
            headers: {
                'Content-Type': 'application/json',
            },
            statusCode: 500,
        });
    });
});
