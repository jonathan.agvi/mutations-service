import { hasMutations, getMutations } from '../../src/utils/mutations.util';

const dnaWithMutations = ['ATGCGA', 'CAGTGC', 'TTATGT', 'AGAAGG', 'CCCCTA', 'TCACTG'];
const dnaWithoutMutations = ['ATGCGA', 'CAGTGC', 'TTATTT', 'AGACGG', 'GCGTCA', 'TCACTG'];

describe('Check if dna has mutation', () => {
    it('Should return true', () => {
        const result = hasMutations(dnaWithMutations);
        expect(result).toBe(true);
    });

    it('Should return false', () => {
        const result = hasMutations(dnaWithoutMutations);
        expect(result).toBe(false);
    });
});

describe('Get mutations from dna array', () => {
    it('Should return 3 results', () => {
        const result = getMutations(dnaWithMutations);
        expect(result).toEqual(['AAAA', 'CCCC', 'GGGG']);
    });

    it('Should return empty array', () => {
        const result = getMutations(dnaWithoutMutations);
        expect(result).toEqual([]);
    });

    it('Should return results', () => {
        const result = getMutations([
            'TAAAAAAAA',
            'GTCCCCCAC',
            'TGTGGGAGG',
            'GTTTTATTT',
            'AAAACAAAA',
            'GCCCGCCCC',
            'CGCGGGGGG',
            'GCTTTTGTT',
            'TATTTTTGT',
        ]);
        expect(result.length).toBe(13);
    });

    it('Should return error "DNA must have at least 4 rows"', () => {
        expect(() => getMutations(['CAT'])).toThrow('DNA must have at least 4 rows');
    });

    it('Should return error "The DNA is incorrectly"', () => {
        const dna = ['AAAA', 'CCCC', 'GGGG', 'TTT'];
        expect(() => getMutations(dna)).toThrow('The DNA is incorrectly');
    });
});
