import { main } from '../../src/handlers/get-mutations-stats';
import { getStats } from '../../src/services/mutations.service';

jest.mock('../../src/services/mutations.service');

describe('Get dna stats', () => {
    it('Should return results successfully', async () => {
        getStats.mockResolvedValue({
            count_mutations: 100,
            count_no_mutations: 10,
            updatedAt: new Date().toISOString(),
        });

        const result = await main();
        expect(getStats).toHaveBeenCalled();
        expect(result.statusCode).toBe(200);
        expect(result.body).toEqual(JSON.stringify({
            message: 'Stats obtained successfully',
            data: { count_mutations: 100, count_no_mutations: 10, ratio: 10 },
        }));
    });

    it('Should return ratio in 0', async () => {
        getStats.mockResolvedValue({
            count_mutations: 0,
            count_no_mutations: 0,
            updatedAt: new Date().toISOString(),
        });

        const result = await main();
        expect(getStats).toHaveBeenCalled();
        expect(result.statusCode).toBe(200);
        expect(result.body).toEqual(JSON.stringify({
            message: 'Stats obtained successfully',
            data: { count_mutations: 0, count_no_mutations: 0, ratio: 0 },
        }));
    });

    it('Should return failure response', async () => {
        getStats.mockRejectedValue(new Error('I still love u!'));
        const result = await main();
        expect(result.statusCode).toBe(500);
        expect(result.body).toEqual(JSON.stringify({
            message: 'Error to get stats',
            errors: 'I still love u!',
        }));
    });
});
