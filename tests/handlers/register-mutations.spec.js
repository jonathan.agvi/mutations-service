import { main } from '../../src/handlers/register-mutations';
import event from '../../data/register-mutations-event.json';
import { increaseMutationsCount, saveDnaList } from '../../src/services/mutations.service';

jest.mock('../../src/services/mutations.service');

describe('Register dna mutations', () => {
    it('Should return true', async () => {
        increaseMutationsCount.mockResolvedValue({
            count_mutations: 1,
            count_no_mutations: 1,
            updatedAt: new Date().toISOString(),
        });
        saveDnaList.mockResolvedValue({
            UnprocessedItems: {},
        });

        const result = await main(event);
        expect(result).toBe(true);
        expect(increaseMutationsCount).toHaveBeenCalledWith(1, 1);
        expect(saveDnaList)
            .toHaveBeenCalledWith([{
                PutRequest: {
                    Item: {
                        dna: ['GAAATAA', 'GGAAAAAG', 'TTGTTATT', 'CCCGACCC', 'AAAAAAAA', 'TTTAATTT'],
                        hasMutations: true,
                        id: 'GAAATAAGGAAAAAGTTGTTATTCCCGACCCAAAAAAAATTTAATTT',
                    },
                },
            }, {
                PutRequest: {
                    Item: {
                        dna: ['ATGCGA', 'CAGTGC', 'TTATTT', 'AGACGG', 'GCGTCA', 'TCACTG'],
                        hasMutations: false,
                        id: 'ATGCGACAGTGCTTATTTAGACGGGCGTCATCACTG',
                    },
                },
            }]);
    });
});
