# Test Guros Mutant DNA
Exámen técnico de Guros para detectar mutaciones en ADN

[![Build Status](https://gitlab.com/jonathan.agvi/mutations-service/badges/master/pipeline.svg)](https://gitlab.com/jonathan.agvi/mutations-service/-/commits/master)

## Required Setup
* Node 14.x
* Serverless Framework v3

## Use

Instalar dependencias:
```
npm i -g serverless@latest
npm i
```

Iniciar proyecto en local:
Antes de correr el proyecto en local, es necesario agregar las siguientes variables al environment .env.local
```
DNA_RESULTS_TABLE_NAME=
DNA_STATS_TABLE_NAME=
DNA_REGISTER_SQS_URL=
```
Y despues ejecturar el siguiente comando
```
npm run local
```

Correr pruebas:
```
npm run test
```

Deploy del proyecto:
```
sls deploy --stage prod
```

## Test Guros
Requerimos que desarrolles un proyecto que detecte si una persona tiene diferencias genéticas basándose en
su secuencia de ADN. Para eso es necesario crear un programa con un método o función con la siguiente
firma:

```
boolean hasMutation(String[] dna);
```

Debe recibir como parámetro un arreglo de cadena caracteres que representan cada fila de una tabla de NxN
con la secuencia del ADN. Las letras de los caracteres solo pueden ser: A, T, C, G; las cuales representan
cada base nitrogenada del ADN.

### Sin mutación:
A T G C G A 

C A G T G C

T T A T T T

A G A C G G

G C G T C A

T C A C T G


### Con mutación:
A T G C G A

C A G T G C

T T A T G T

A G A A G G

C C C C T A

T C A C T G



Sabrás si existe una mutación si se encuentra más de una secuencia de cuatro letras iguales, de forma oblicua,
horizontal o vertical.

Ejemplo de caso con mutación:
```
String[] dna = {"ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"};
```

En este caso el llamado a la función isMutant(dna) devuelve “true”.
Desarrolla el algoritmo de la manera más eficiente posible.

# Desafíos:

## Nivel 1:
Programa (en cualquier lenguaje de programación) que cumpla con lo descrito anteriormente.

## Nivel 2:
Crear una API REST, hostear esa API en un cloud computing libre (AWS, Azure DevOps, GCP, etc.), crear el
endpoint POST /mutation/ en donde se pueda detectar si existe mutación enviando la secuencia de ADN
mediante un JSON el cual tenga el siguiente formato:

```
POST → /mutation
{
“dna”:["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"]
}
```

En caso de verificar una mutación, debería devolver un 200, en caso contrario un 403.

## Nivel 3:
Anexar una base de datos, la cual guarde los ADNs verificados con la API. Sólo 1 registro por ADN. Exponer un
servicio extra GET /stats que devuelva un JSON con las estadísticas de las verificaciones de ADN:
```
GET → /mutation/stats
{
"count_mutations": 40,
"count_no_mutation": 100,
"ratio": 0.4
}
```
