module.exports = {
    plugins: ['jest', 'import'],
    env: {
        es6: true,
        node: true,
        'jest/globals': true,
    },
    extends: [
        'airbnb-base',
        'plugin:jest/recommended',
        'plugin:import/recommended',
    ],
    parserOptions: {
        ecmaVersion: 2018,
        sourceType: 'module',
    },
    rules: {
        camelcase: 'off',
        'import/no-extraneous-dependencies': 'off',
        'no-plusplus': ['error', { allowForLoopAfterthoughts: true }],
        indent: ['error', 4, { SwitchCase: 1 }],
        'no-underscore-dangle': 'off',
        'import/prefer-default-export': 'off',
        'max-len': [
            'error',
            {
                code: 140,
                tabWidth: 4,
                ignoreComments: true,
                ignoreUrls: true,
            },
        ],
    },
};
